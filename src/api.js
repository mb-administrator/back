const express = require("express");
const serverless = require("serverless-http");
const { connectDb } = require("./db");

const app = express();
const router = express.Router();

router.get("/", (req, res) => {
    res.status(200).json({
        routes: [{
            name: 'Mario Eduardoo Contreras Serrano',
        }, {
            name: 'Rodrigo Miranda Robles'
        }]
    });
});

router.get("/test", (req, res) => {
    res.status(200).json({
        hello: "test"
    });
});

app.use(`/.netlify/functions/api`, router);

connectDb().then(() => {
    console.log(`Example app conected!`);
});

module.exports = app;
module.exports.handler = serverless(app);